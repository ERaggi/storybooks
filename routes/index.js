const express = require('express');
const router = express.Router();
const { ensureAuth, ensureGuest } = require('../middleware/auth')

const Story = require('../models/Story');


router.get('/login', ensureGuest, (req, res) => {
    //res.send('login')
    res.render('login', {
        layout: 'login'});
})

router.get('/dashboard', ensureAuth, async (req, res) => {
    // console.log(req.user);

    try {
        const stories = await Story.find({ user: req.user.id }).lean()
        res.render('dashboard', {
            name: req.user.firstName,
            stories
        });    
    } catch (error) {
        console.error(error)
        res.error('error/500')
    }
})

router.get('/additionalpage', (req, res) => {
    res.render('additionalpage');
})

module.exports = router