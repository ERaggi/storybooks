const express = require('express');
const passport = require('passport');
const router = express.Router();

// Auth with google
// GET /auth/google

router.get('/google', passport.authenticate('google', { scope: ['profile'] }))

// Google Auth Callback
// GET /auth/google/callback

router.get('/google/callback', passport.authenticate('google', { failureRedirect: '/'}), (req, res) => {
    res.redirect('/dashboard');
})

router.get('/additionalpage', (req, res) => {
    res.render('additionalpage');
})

// Logout user
// /auth/logout

router.get('/logout', (req, res) => {
    req.logout();
    res.redirect('/login');
})


module.exports = router